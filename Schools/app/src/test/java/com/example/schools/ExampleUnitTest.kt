package com.example.schools

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var schoolDataService: SchoolDataService

    private lateinit var mainViewModel: MainViewModel

    @Before
    fun setUp() {
        Dispatchers.setMain(Dispatchers.Unconfined) // Use Unconfined for testing
        mainViewModel = MainViewModel()
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // Reset after tests
    }

    @Test
    fun `fetch data success`() = runBlocking {
        val mockData = listOf(SchoolInfo("21K728", "Liberation Diploma Plus High School", "The mission of Liberation Diploma Plus High School, in partnership with CAMBA, is to develop the student academically, socially, and emotionally. We will equip students with the skills needed to evaluate their options so that they can make informed and appropriate choices and create personal goals for success. Our year-round model (trimesters plus summer school) provides students the opportunity to gain credits and attain required graduation competencies at an accelerated rate. Our partners offer all students career preparation and college exposure. Students have the opportunity to earn college credit(s). In addition to fulfilling New York City graduation requirements, students are required to complete a portfolio to receive a high school diploma.", "718-946-6812", "scaraway@schools.nyc.gov", "2865 West 19th Street, Brooklyn, NY 11224 (40.576976, -73.985413)", "Basketball "))
        `when`(schoolDataService.pullData()).thenReturn(mockData)

        mainViewModel.fetchData()

        assertEquals(mockData, mainViewModel.schools.value)
    }

    @Test
    fun `fetch data failure`() = runBlocking {
        `when`(schoolDataService.pullData()).thenThrow(RuntimeException("Failed to fetch"))

        mainViewModel.fetchData()

        assertEquals(emptyList<SchoolInfo>(), mainViewModel.schools.value)
    }
}
