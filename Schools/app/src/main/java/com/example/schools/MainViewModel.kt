package com.example.schools

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainViewModel(): ViewModel() {

    private val _schools = MutableLiveData<List<SchoolInfo>>()
    val schools: LiveData<List<SchoolInfo>> = _schools

    val URL = "https://data.cityofnewyork.us/resource/"

    init {
        fetchData()
    }

    fun createRetrofitService(): SchoolDataService {
        val retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create(SchoolDataService::class.java)
    }

    fun fetchData() {
        viewModelScope.launch {
            val service = createRetrofitService()
            try {
                val data = service.pullData()
                _schools.postValue(data)
            } catch (e: Exception) {
                _schools.postValue(emptyList())
            }
        }
    }
}