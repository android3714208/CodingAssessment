package com.example.schools

data class SchoolInfo (val dbn: String,
                       val school_name: String,
                       val overview_paragraph: String,
                       val phone_number: String?,
                       val school_email: String?,
                       val location: String?,
                       val school_sports: String?)
