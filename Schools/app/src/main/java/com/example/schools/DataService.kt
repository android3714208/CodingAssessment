package com.example.schools

import retrofit2.http.GET

interface SchoolDataService {
    @GET("s3k6-pzi2.json")
    suspend fun pullData(): List<SchoolInfo>
}