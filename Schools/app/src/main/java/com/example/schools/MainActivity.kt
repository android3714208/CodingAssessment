package com.example.schools

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModelProvider
import com.example.schools.ui.theme.SchoolsTheme


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        setContent {
            SchoolsTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    SchoolListScreen(viewModel)
                }
            }
        }
    }
}

@Composable
fun SchoolListScreen(viewModel: MainViewModel) {
    val schoolList by viewModel.schools.observeAsState(initial = emptyList())
    MyApp(schoolList)
}

@Composable
fun MyApp(schoolList: List<SchoolInfo>) {
    var schoolSelected by remember {
        mutableStateOf<SchoolInfo?>(null)
    }
    val lazyListState = rememberLazyListState()
    val modifier = Modifier.padding(16.dp)
    if (schoolSelected==null) {
        DisplaySchoolList(schoolList, lazyListState, modifier) {
            school -> schoolSelected = school
        }
    }
    else {
        DisplaySchoolInfo(schoolSelected!!, modifier) {
            schoolSelected = null
        }
    }
}


// Display's School List
@Composable
fun DisplaySchoolList(schoolList: List<SchoolInfo>, lazyListState: LazyListState, modifier: Modifier, onSchoolSelected: (SchoolInfo)->Unit) {
    LazyColumn(state = lazyListState) {
        items(schoolList) {
            school ->
            Text(text = "${school.school_name} \nDBN:${school.dbn}",
                fontSize = 24.sp,
                modifier = modifier.clickable { onSchoolSelected(school) }
            )
            Divider()
        }
    }
}


// Display's School Information
@Composable
fun DisplaySchoolInfo(schoolSelected: SchoolInfo, modifier: Modifier, backToList: () -> Unit) {
    BackHandler {
        backToList()
    }
    val infoList = listOf(
        "School Name" to schoolSelected.school_name,
        "Overview" to schoolSelected.overview_paragraph,
        "Email" to (schoolSelected.school_email ?: "Email Not Provided"),
        "Phone Number" to (schoolSelected.phone_number ?: "Phone Number Not Provided"),
        "Sports" to (schoolSelected.school_sports ?: "Sports Not Provided"),
        "Address" to (schoolSelected.location ?: "Location Not Provided")
    )

    Box(modifier = modifier.verticalScroll(rememberScrollState())) {
        Column {
            infoList.forEach{
                    (label, content) ->
                    Text(text = "$label \n$content", fontSize = 24.sp, modifier = modifier)
                    Divider()
            }
            Button(onClick = { backToList() }) {
                Image(imageVector = Icons.Default.ArrowBack, contentDescription = null)
            }
        }
    }
}